<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// BEGIN display infor route
Route::get('/', 'MainController@index')->name('home')->middleware('auth');

// BEGIN form add user route
Route::get('/createform', 'MainController@CreateForm')->name('createForm');
Route::post('/formadduseraction', 'MainController@formAddUserAction')->name('FormAddUserAction');
Route::get('/view/{id}', 'MainController@view')->name("view");
Route::get('/update/update/{id}', 'MainController@update')->name("update");
Route::put('/edit/{id}', 'MainController@edit')->name("edit");
Route::get('/delete/{id}', 'MainController@delete')->name("delete");
Route::get('/search', 'MainController@Search')->name("search");

// BEGIN traslate language
Route::get('/khmer', 'MainController@khmer')->name("khmer");
Route::get('/english', 'MainController@english')->name("english");

// BEGIN about us page route
Route::get('/aboutus', 'MainController@aboutUs')->name("aboutUs")->middleware('auth');

// BEGIN send email route
Route::get('/sendmail', 'MailController@sendMail')->name("sendMail");



Auth::routes();
// Auth::routes(["register"=> false]);

Route::get('/home', 'HomeController@index')->name('home');

//Get user information from database
Route::get('/guestInfo', 'MainController@guestInfo');

// logout route
Route::get('/logout', 'MainController@logout');
Route::get('/logoutall', 'MainController@logoutall');