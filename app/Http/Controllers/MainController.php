<?php

namespace App\Http\Controllers;
use App\UserModel;
use Illuminate\Http\Request;
use App\Http\Requests\RequestInsert;
use Auth;

class MainController extends Controller
{
    
    // BEGIN function display user inforamtion on dashboard
    public function index(){
        $files = "";
        $filter = "";
        $startDate = "";
        $endDate = "";

        $users = UserModel::orderby('id', 'desc')->paginate(6);
        $count_row = UserModel::paginate(6)->count();
        $filters = array("id"=>"ID", "name"=>"Full Name", "email"=>"Email Address", "phone_number"=>"Phone Number");
        return view('pages.index')->with('users', $users)->with('count_row', $count_row)->with('filters', $filters)->with('filter', $filter)->with('startDate', $startDate)->with('endDate', $endDate);
    }

    // BEGIN function return to form add 
    public function CreateForm() {
        return view('pages.formAddUser');
    }

    // BEGIN function add user to database
    public function formAddUserAction(RequestInsert $request) {
        
        if($request->hasFile('profileName')){
            $files = $request->file("profileName");
            $fileDesination = "public/upload";
            $imageFileNameURl = str_replace("public", "", $files->store($fileDesination));

            $files1 = $request->file("profileName1");
            $fileDesination1 = "public/upload";
            $imageFileNameURl1 = str_replace("public", "", $files1->store($fileDesination1));

            $user = new UserModel();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->profile = $imageFileNameURl;
            $user->profile1 = $imageFileNameURl1;
            $user->save();
            return redirect('/');
        }else {

            $imageFileNameURl = "/upload/defaultUser.png";
            $user = new UserModel();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->profile = $imageFileNameURl;
            $user->save();
            return redirect('/');
        }
    }

    // BEGIN function edit user infromation
    public function Update($id){
        $user = UserModel::find($id);
        return view('pages.editUser')->with('user', $user);
    }

    public function edit($id, Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
        ]);

        if ($request->hasFile('profileName')){
            // This block working when has file upload
            $files = $request->file("profileName");
            $fileDesination = "public/upload";
            $imageFileNameURl = str_replace("public", "", $files->store($fileDesination));

            $user = UserModel::Find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->profile = $imageFileNameURl;
            $user->save();
            return redirect()->route("home");

        } else {
            // This block working when no file upload
            $user = UserModel::Find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->save();
            return redirect()->route("home");
        }
    }

    // BEGIN View user information
    public function view($id){
        $user = UserModel::find($id);
 
         return view('pages.view', compact('user'));
     }

    // BEGIN delete function 
    public function delete($id){
        UserModel::where('id', $id)->delete();
        return redirect()->route("home");
    }

    // BEGIN Function search user information
    public function Search(Request $request){
        $Search = $request->input('txtSearch');
        $ValueInfo = $request->input('txtValueInfo');
        $keyword = $request->txtSearch;
        $filter = $request->txtValueInfo;
        $startDate = $request->input('txtStartDate');
        $endDate = $request->input('txtEndDate');

        $users = UserModel::orwhere($ValueInfo, 'like', '%'.$Search.'%')
                            ->wherebetween('created_at', [$startDate, $endDate])
                            ->paginate(5);
        $count_row = $users->count();
        $filter = $request->txtValueInfo;
        $filters = array("id"=>"ID", "name"=>"Full Name", "email"=>"Email Address", "phone_number"=>"Phone Number");
        return view('pages.index', compact('users','count_row','filters', 'filter', 'keyword', 'startDate', 'endDate'));
    }

    // BEGIN Function translate
    public function khmer(){
        \App::setlocale("kh");
        \Session::put("locale", "kh");
        return redirect()->back();
    }
    public function english(){
        \App::setlocale("en");
        \Session::put("locale", "en");
        return redirect()->back();
    }

    // BEGIN Function about us page
    public function aboutUs(){
        return view('pages.aboutUs');
    }

    // Get user information
    public function guestInfo(Request $request){
        echo $request->user()->name;
    }

    // Function logout 
    public function logout(){
        Auth::logout();
        return redirect()->route('home');
    }

    //function logout all other device
    public function logoutall(){
        Auth::logoutOtherDevices('Pa$$w0rd123');
        // return rediect()->route('home');
    }
}
