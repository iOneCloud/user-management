<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
    // BEGIN function send mail
    public function sendMail(){
        //     Mail::raw("Hello from me!", function($message){
        //         $message->to("chuonraksa007@gmail.com")->subject("Register");
        //         $message->from("no_reply@ione2u.com", "Register User");
        // });
    
        $data = array(
            "id" => "001",
            "name" => "Chuon Raksa",
            "email" => "Chuon Raksa",
            "phone" => "010 73 70 87"
        );

        Mail::send('pages.mailTemplate', $data, function($message){
            $message->to("chuonraksa007@gmail.com")->subject("Register");
            $message->from("no_reply@ione2u.com", "Register User");
        });
}
}
