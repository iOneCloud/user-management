<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class RequestInsert extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:5|unique:tbl_users',
            'email' => 'required',
            'phone_number' => 'required',
        ];
    }

    public function messages(){
        return [
            "name.required" => __('message.information_cannot_be_null'),
            "name.max" => __('message.name_cannot_more_than_5_char'),
            "name.unique" => __('message.username_already_have')
        ];
    }
}
