
@extends('layouts.default')
@section('content')    
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="{{asset('sb-admin-2.css')}}">
        <title>Edit User Information</title>
    </head>
    <body>
            <!-- BEGIN form upload user information -->
            <div class="col-lg-6">
                <h1 class="h5 mb-0 text-gray-800">@lang('message.update_user_information')</h1><br>
                    <form action="{{route('edit', $user->id)}}" method="post" enctype="multipart/form-data">
                        @method("PUT")
                        @csrf
                        <div class="form-group">
                            @error('name')
                                <label style="color: #ca0000;">@lang('message.information_cannot_be_null')</label>
                            @enderror 
                            <input type="text" class="form-control form-control-user" name="name" value="{{$user->name}}" placeholder="Enter full name">
                        </div>
                        <div class="form-group">
                            @error('email')
                                <label style="color: #ca0000;">@lang('message.information_cannot_be_null')</label>
                            @enderror
                            <input type="email" class="form-control form-control-user" name="email" value="{{$user->email}}" placeholder="Enter Email Address...">
                        </div>
                        <div class="form-group">
                            @error('phone_number')
                                <label style="color: #ca0000;">@lang('message.information_cannot_be_null')</label>
                            @enderror
                            <input type="text" class="form-control form-control-user" name="phone_number" value="{{$user->phone_number}}" placeholder="Enter Phone number">
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control form-control-user" name="profileName" placeholder="Select Profile">
                        </div>
                        <div class="form-group">
                            <img src="/storage/{{$user->profile}}" style="width:150px; height:150px;" class="rounded-circle">
                        </div>
                            <input  type="submit" value="@lang('message.button_update_information')" class="btn btn-primary btn-user btn-block"><br>        
                    </form>          
                </div>
            </div>
            <!-- END form upload user information -->
    </body>
    </html>
@endsection


