@extends('layouts.default')
@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Home</title>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- BEGIN Function switch alert -->
        <script type="text/javascript">
            function confirDelete(id) {
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        window.location.href="/delete/"+id;
                        swal("Poof! Your record has been deleted!", {
                        icon: "success",
                        });
                    } else {
                        swal("Your record is safe!");
                    }
                });
            }
        </script>
        <!-- ENG Function switch alert -->
    </head>
    <body>
        <!-- BEGIN form search for the top -->
        <form action="{{route('search')}}" method="GET">
            <table>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <label>@lang('message.search')</label>
                        <input type="text" class="form-control" name="txtSearch" value="{{isset($keyword)?$keyword:''}}" required>
                    </td>
                    <td>
                        <label>@lang('message.filter')</label>
                        <select class="form-control" name='txtValueInfo'>
                        @foreach($filters as $f => $f_value)
                        <option value="{{$f}}"
                        @if($f == $filter)
                                selected = "selected"
                        @endif
                        >{{$f_value}}</option>
                        @endforeach
                        </select>
                    </td>
                    <td>
                        <label>@lang('message.start_date')</label>
                        <input type="date" class="form-control" id="formGroupExampleInput" value="{{isset($startDate)?$startDate:''}}" placeholder="Example input" name="txtStartDate">
                    </td>
                    <td>
                        <label>@lang('message.end_date')</label> 
                        <input type="date" class="form-control" id="formGroupExampleInput" value="{{isset($endDate)?$endDate:''}}" placeholder="Example input" name="txtEndDate">
                    </td>
                    <td>
                        <label></label><br> 
                        <button style="margin-top: 8px;" type="submit"class="form-control btn btn-primary" name="btn_find_more" style="width: 285px;">@lang('message.search_information')</button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- END form search for the top -->
        <!-- BEGIN using loop for display user information -->
        <br>
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>@lang('message.id')</th>
                    <th>@lang('message.full_name')</th>
                    <th>@lang('message.email')</th>
                    <th>@lang('message.phone_number')</th>
                    <th style="text-align: center;">@lang('message.profile')</th>
                    <th colspan="4" style="text-align: center;">@lang('message.action')</th>
                </tr>
            </thead>
            @if(count($users))
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td style="text-align: center;"><img src="/storage/{{$user->profile}}" class="rounded-circle" style="width:55px; height:55px;"></td>
                    <td><a href="{{route('view', $user->id)}}"><button type="submit" class="btn btn-primary btn-circle"><i class="fas fa-eye"></i></button></td>
                    <td><a href="{{route('update', $user->id)}}"><button type="submit" class="btn btn-warning btn-circle"><i class="fas fa-edit"></i></button></td>
                    <td><a href="#"><button type="button" onclick="confirDelete({{$user->id}});" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></button></td>
                </tr>
            @endforeach
            @endif
            @if(!count($users))
                <tr>
                    <td>No User information<td>
                    <td><td>
                    <td><td>
                </tr>
            @endif
        </table> 
        <!-- Display pagination -->
        {{$users->appends(request()->all())->links()}}
        @lang('message.total_row'): <b>{{$count_row}}</b><br><br>
        <!-- END using loop for display user information -->
        </div>
    </body>
    </html>
@endsection


