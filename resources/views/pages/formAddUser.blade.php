@extends('layouts.default')
@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="{{asset('sb-admin-2.css')}}">
        <title>Form Add User Information</title>
    </head>
    <body>
        <!-- BEGIN form upload user information -->
        <div class="col-lg-6">
            <h1 class="h5 mb-0 text-gray-800">@lang('message.add_new_user_information')</h1><br>
                <form action="/formadduseraction" method="post" class="user" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        @if($errors->has('name'))
                        <label style="color: #ca0000;">{{$errors->first('name')}}</label>
                        @endif
                        <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="@lang('message.placeholder_enter_full_name')">
                    </div>
                    <div class="form-group">
                        @error('email')
                            <label style="color: #ca0000;">@lang('message.information_cannot_be_null')</label>
                        @enderror
                        <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="@lang('message.placeholder_email_address')">
                    </div>
                    <div class="form-group">
                        @error('phone_number')
                            <label style="color: #ca0000;">@lang('message.information_cannot_be_null')</label>
                        @enderror
                        <input type="text" class="form-control" name="phone_number" value="{{old('phone_number')}}" placeholder="@lang('message.placeholder_enter_phone_number')">
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control" name="profileName" placeholder="Enter profile">
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control" name="profileName1" placeholder="Enter profile">
                    </div>
                    <input  type="submit" value="@lang('message.button_submit_info')" class="btn btn-primary btn-block">        
                </form>        
            </div>
        </div>
        <!-- END form upload user information -->
    </body>
    </html>
@endsection