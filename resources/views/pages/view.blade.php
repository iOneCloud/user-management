
@extends('layouts.default')
@section('content')    
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="{{asset('sb-admin-2.css')}}">
        <title>View User Information</title>
    </head>
    <body>
    <!-- BEGIN display user information -->
    <div style="width: 600px;">    
        <table class="table">
            <tr>
                <td><b>@lang('message.id') : </b> {{$user->id}}</td>
            </tr>
            <tr>
                <td><b>@lang('message.full_name') : </b> {{$user->name}}</td>
            </tr>
            <tr>
                <td><b>@lang('message.email') : </b> {{$user->email}}</td>
            </tr>
            <tr>
                <td><b>@lang('message.phone_number') : </b> {{$user->phone_number}}</td>
            </tr>
            <tr>
                <td><b>@lang('message.profile') : </b> </td>
            </tr>
            <tr>
                <td>
                    <img src="/storage/{{$user->profile}}" style="width:150px; height:150px;" class="rounded-circle">
                </td>
            </tr>
        </table>
        <a href="{{route('home')}}"><button type="submit" class="btn btn-primary">@lang('message.button_back')</button><br><br></a>
    </div>
    </div>
</body>
</html>
@endsection


