@extends('layouts.default')
@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>About Us</title>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <body>
        <!-- BEGIN display about us content -->
        <h1 class="h5 mb-0 text-gray-800">@lang('message.about_us')</h1><br>
            <p>Korea Software HRD Center is located at Sangkat Boeung Kak II, KhanToul Kork, Phnom Penh, Cambodia. 80 students are selected every year from local universities. It is the first center in Cambodia that provides a lot of benefits to students, which other centers could not do it. There are two main courses in training curriculum; basic and advanced course. Basic course includes Java programming and web development training. In advanced course, students perform individual group project.</p>
        </div>
    </body>
    </html>
@endsection


