<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="{{asset('all.min .css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{asset('sb-admin-2.css')}}">
    <title></title>
</head>
<body>
    <!-- BEGIN footer navigation -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto" style="color: #999;">
            <span>@lang('message.copyright')</span>
          </div>
        </div>
      </footer>
    <!-- END footer navigation -->
</body>
</html>