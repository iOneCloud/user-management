
@include('includes.header')

    <!-- BEGIN Display content  -->
    <div class="container-fluid">          
        @yield('content')
    </div>
    <!-- END Display content -->

@include('includes.footer')